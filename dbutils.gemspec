$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "dbutils/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "dbutils"
  s.version     = Dbutils::VERSION
  s.authors     = ["Manuel Burato"]
  s.email       = ["contact@manuelburato.it"]
  s.homepage    = ""
  s.summary     = "A collection of DataBase UTILities"
  s.description = s.summary
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "> 5.0.0"
  s.add_dependency "mysql2"

  s.add_development_dependency "byebug"
end
