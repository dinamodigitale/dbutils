class Dbutils::Railtie < Rails::Railtie
  rake_tasks do
    load 'tasks/dbutils_tasks.rake'
  end
end
