require "optparse"

desc "DB Utils expose a collection of Quality-of-life tasks for managing rails db and preserve Manuel's mental health"
namespace :db do |args|
  desc "Totally destroy database and re-init using migration system instead of schema.rb"
  task razzate: :environment do
    if Rails.env.production?
      print "Your are in a production environement! This operation is going to make lot of damage, are you surely sure to proceed? (y/N)"
      input = STDIN.gets.chomp
      if input.downcase != "y" and input.downcase != "yes"
        puts "ok ciao"
        exit
      end
    end

    puts "Cleaning Temp".bold
    Rake::Task["tmp:clear"].invoke

    puts "Setting Env".bold
    Rake::Task["db:environment:set"].invoke

    puts "Dropping DB".bold
    Rake::Task["db:drop"].invoke

    puts "Creating DB".bold
    Rake::Task["db:create"].invoke

    puts "Migrating DB".bold
    Rake::Task["db:migrate"].invoke

    puts "Cleaning Public Assets".bold
    FileUtils.rm_rf Rails.root.join("public", "system")

    if File.exist?(Rails.root.join("private"))
      puts "Cleaning Private Assets".bold
      FileUtils.rm_rf(Rails.root.join("private", "system"))
    end

    if defined?(Comunator)
      if tsk = Rake::Task["comunator:update"]
        puts "Comunator update".bold
        tsk.invoke
      end
    end

    if Dbutils.respond_to?(:before_seed)
      puts "Invoking custom before_seed procedure".bold
      Dbutils.before_seed(Rake::Task)
    end

    puts "Seeding DB".bold
    Rake::Task["db:seed"].invoke

    puts "DONE".bold
  end

  desc "Dump db structure and data to dump.sql through mysqldump command. You can choose destination file with -o param"
  task dump: :environment do
    config = Rails.configuration.database_configuration
    host = config[Rails.env]["host"] || "localhost"
    database = config[Rails.env]["database"]
    username = config[Rails.env]["username"]
    password = config[Rails.env]["password"]

    cmd_args = {
      h: host,
      u: username,
    }
    cmd_args[:p] = password if password

    cmd_args_string = ""
    cmd_args.each do |k, v|
      cmd_args_string << "-#{k}#{v} "
    end

    options = {
      outfile: Rails.root.join("db", "dump.sql"),
    }

    option_parser = OptionParser.new do |opts|
      opts.banner = "Usage: rake dbutils:dump [options]"
      opts.on("-o", "--out-file FILE", "Specify a different outfile", String) do |outfile|
        options[:outfile] = outfile
      end
    end
    args = option_parser.order!(ARGV) { }
    option_parser.parse!(args)

    puts "Dumping '#{database}' structure and data to '#{options[:outfile]}'"

    `mysqldump #{cmd_args_string} #{database} > #{options[:outfile]}`
  end

  namespace :dump do
    task restore: :environment do
      config = Rails.configuration.database_configuration
      host = config[Rails.env]["host"] || "localhost"
      database = config[Rails.env]["database"]
      username = config[Rails.env]["username"]
      password = config[Rails.env]["password"]

      cmd_args = {
        h: host,
        u: username,
      }
      cmd_args[:p] = password if password

      cmd_args_string = ""
      cmd_args.each do |k, v|
        cmd_args_string << "-#{k}#{v} "
      end

      options = {
        inputfile: Rails.root.join("db", "dump.sql"),
      }

      option_parser = OptionParser.new do |opts|
        opts.banner = "Usage: rake dbutils:dump [options]"
        opts.on("-i", "--in-file FILE", "Specify a different input file", String) do |inputfile|
          options[:inputfile] = inputfile
        end
      end
      args = option_parser.order!(ARGV) { }
      option_parser.parse!(args)

      puts "Restoring '#{options[:inputfile]}' structure and data to '#{database}'"

      `mysql #{cmd_args_string} #{database} < #{options[:inputfile]}`
    end
  end

  namespace :razzate do
    desc "Truncate all tables preserving db structure. Also run seed at the end"
    task data: :environment do
      puts "Truncating Data".bold
      ApplicationRecord.connection.execute("SET foreign_key_checks = 0")

      ApplicationRecord.connection.tables.each do |table|
        if table.in?(["ar_internal_metadata", "schema_migrations"])
          puts "Skipping #{table}"
          next
        end
        puts query = "TRUNCATE #{table}"
        ApplicationRecord.connection.execute(query)
      end

      ApplicationRecord.connection.execute("SET foreign_key_checks = 1")

      puts "Cleaning Assets".bold
      FileUtils.rm_rf Rails.root.join("public", "system")

      if File.exist?(Rails.root.join("private"))
        puts "Cleaning Private Assets".bold
        FileUtils.rm_rf(Rails.root.join("private", "system"))
      end

      if defined?(Comunator)
        if tsk = Rake::Task["comunator:update"]
          puts "Comunator update".bold
          tsk.invoke
        end
      end

      puts "Seeding DB".bold
      Rake::Task["db:seed"].invoke

      puts "DONE".bold
    end
  end
end
